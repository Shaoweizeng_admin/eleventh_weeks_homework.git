# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from urllib.parse import quote
import time
from douban.items import MasterItem


class BookSpider(scrapy.Spider):
    name = 'book'
    allowed_domains = ['book.douban.com']
    start_urls = ['https://book.douban.com/tag/?view=type&icn=index-sorttags-all']

    def parse(self, response):
        lists = response.selector.css("table.tagCol")
        for alist in lists:
            items = alist.css("td")
            for item in items:
                category = item.css("a::attr('href')").extract_first()
                url = "https://book.douban.com" + quote(category)
                yield Request(url=url, callback=self.parseBookUrl, dont_filter=True)


    def parseBookUrl(self, response):
        lists = response.selector.css("li.subject-item")
        for alist in lists:
            bookurl = alist.css("a.nbg::attr('href')").extract_first()          
            item = MasterItem()
            item['url'] = bookurl
            yield item
        if response.selector.css("span.next"):
            category = response.selector.css("span.next a::attr('href')").extract_first()
            url = "https://book.douban.com" + category
            #print(url)
            time.sleep(2)
            yield Request(url=url, callback=self.parseBookUrl, dont_filter=True)