from django.conf.urls import url
from django.views.static import serve
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^douban/showList/(?P<pIndex>[0-9]+)$', views.showList, name="showlist"),
    url(r'^douban/showChart$', views.showchart, name="showchart"),
]