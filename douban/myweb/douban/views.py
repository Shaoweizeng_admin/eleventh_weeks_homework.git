from django.shortcuts import render
from django.http import HttpResponse
from douban.models import Books
import os, time
from datetime import datetime
from django.core.paginator import Paginator
from django.db.models import Q

# Create your views here.
def index(request):
    return render(request, "douban/index.html")


# 显示图书列表
def showList(request, pIndex):
    list = Books.objects.all()
    p = Paginator(list, 1000)
    if pIndex == "":
        pIndex = 1
    list2 = p.page(pIndex)
    plist = p.page_range
    context = {"bookList":list2,"plist":plist,"pIndex":int(pIndex)}
    return render(request, "douban/main.html", context)


def showchart(request):
    years = []
    records = []
    for i in range(2000, 2019):
        years.append(''+str(i)+'')
        records.append(str(Books.objects.filter(Q(publishdate__startswith=''+ str(i) + '')).count()))

    scores = []
    counts = []
    for s in range(0, 10):
        scores.append(''+str(s)+'')
        counts.append(str(Books.objects.filter(Q(score__startswith=''+ str(s) + '')).count()))

    context = {"years":",".join(years), "records":",".join(records), "scores":",".join(scores), "counts":",".join(counts)}
    return render(request, "douban/chart.html", context)