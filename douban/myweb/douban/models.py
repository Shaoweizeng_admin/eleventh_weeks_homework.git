from django.db import models
from datetime import datetime

# Create your models here.
class Books(models.Model):
    id = models.AutoField(primary_key = True )
    bookid = models.CharField(max_length = 16)
    bookname = models.CharField(max_length = 150)
    author = models.CharField(max_length = 150)
    publisher = models.CharField(max_length = 150)
    original = models.CharField(max_length = 150)
    translator = models.CharField(max_length = 150)
    publishdate = models.CharField(max_length = 100)
    pages = models.IntegerField(max_length=11)
    price = models.IntegerField(max_length=11)
    binding = models.CharField(max_length = 150)
    series = models.CharField(max_length = 150)
    isbn = models.CharField(max_length = 50)
    score = models.CharField(max_length = 10)
    number = models.IntegerField(max_length=11)

    def __str__(self):
        return "%d:%s:%s"%(self.bookid,self.bookname,self.author)

    class Meta:
        db_table = "Books"