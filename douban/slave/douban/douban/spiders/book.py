# -*- coding: utf-8 -*-
import scrapy
from scrapy_redis.spiders import RedisSpider
from douban.items import BookItem 
import time
import re


class BookSpider(RedisSpider):
    name = 'book'
    redis_key = "bookspider:start_urls"

    def __init__(self, *args, **kwargs):
        # Dynamically define the allowed domains list.
        domain = kwargs.pop('domain', '')
        self.allowed_domains = filter(None, domain.split(','))
        super(BookSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        item = BookItem()
        item['bookid'] = response.url.split("/")[-2]
        item['bookname'] = response.css("h1 span::text").extract_first().strip()
        if response.xpath("//div[@id='info']/a[1]/text()").extract_first():
            item['author'] = response.xpath("//div[@id='info']/a[1]/text()").extract_first().replace("\n            ", " ").strip()
        item['publisher'] = " ".join(re.findall("出版社:</span>\s*(.*?)<br>", response.css("#info").extract_first()))
        item['original'] = " ".join(re.findall("原作名:</span>\s*(.*?)<br>", response.css("#info").extract_first()))
        if re.findall("译者</span>:\s*(.*?)>(.*?)</a>", response.css("#info").extract_first()):
            item['translator'] = " ".join(re.findall("译者</span>:\s*(.*?)>(.*?)</a>", response.css("#info").extract_first())[0][1])
        item['publishdate'] = " ".join(re.findall("出版年:</span>\s*(.*?)<br>", response.css("#info").extract_first()))
        item['pages'] = " ".join(re.findall("页数:</span>\s*(.*?)<br>", response.css("#info").extract_first()))
        item['price'] = " ".join(re.findall("定价:</span>\s*(.*?)<br>", response.css("#info").extract_first()))
        item['binding'] = " ".join(re.findall("装帧:</span>\s*(.*?)<br>", response.css("#info").extract_first()))
        if re.findall("丛书:</span>\s*(.*?)>(.*?)</a>", response.css("#info").extract_first()):
            item['series'] = re.findall("丛书:</span>\s*(.*?)>(.*?)</a>", response.css("#info").extract_first())[0][1]
        item['isbn'] = " ".join(re.findall("ISBN:</span>\s*(.*?)<br>", response.css("#info").extract_first()))
        if response.css("strong.rating_num::text").extract_first():
            item['score'] = response.css("strong.rating_num::text").extract_first().strip()
        if response.css("a.rating_people span::text").extract_first():
            item['number'] = response.css("a.rating_people span::text").extract_first().strip()
        #print(item)
        yield item
        