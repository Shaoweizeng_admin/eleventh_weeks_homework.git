# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymysql
from scrapy import Request
from scrapy.exceptions import DropItem
from scrapy.utils.project import get_project_settings


class DoubanPipeline(object):
    def process_item(self, item, spider):
        return item


class MysqlPipeline(object):
    def __init__(self,host,user,password,database,port):
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.port = port
    
    @classmethod
    def from_crawler(cls,crawler):
        return cls(
            host = crawler.settings.get("MYSQL_HOST"),
            user = crawler.settings.get("MYSQL_USER"),
            password = crawler.settings.get("MYSQL_PASS"),
            database = crawler.settings.get("MYSQL_DATABASE"),
            port = crawler.settings.get("MYSQL_PORT"),
        )

    def open_spider(self, spider):
        '''负责连接数据库'''
        self.db = pymysql.connect(self.host,self.user,self.password,self.database,charset="utf8",port=self.port)
        self.cursor = self.db.cursor()
        

    '''执行数据表的写入操作'''
    def process_item(self, item, spider):
        try:
            data = dict(item)
            keys = ", ".join(data.keys())
            values = ", ".join(['%s']*len(data))
            sql = "insert into %s(%s) values (%s)"%(item.tablename, keys, values)
            self.cursor.execute(sql, tuple(data.values()))
            self.db.commit()
        except pymysql.err.IntegrityError:
            pass
        except Exception as Err:
            print(Err.value)
        return item

    def close_spider(self, spider):
        '''关闭连接数据库'''
        self.db.close()

